using Mach1;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;

public enum SpatialFormats 
{
    M1Spatial = 5,
    Mono = 13,
    Stereo = 14,
    FuMa = 99,
    ACNSN3D = 100,
    TBE = 101,
    Ambix_3rd_Order = 104
}

public class M1SpatialTranscode : MonoBehaviour
{
    public AudioClip clip;
    public string audioFileName;
    public SpatialFormats inputFormat, outputFormat;

    public List<AudioClip> convertedClip;
    [SerializeField] M1SpatialDecode decoder;

    List<float[]> inBuffer;
    
    private void Start()
    {
        StartCoroutine(LoadAudioFromStreamingAssets(audioFileName));
    }

    /// <summary>
    /// Loads Load audio from streaming assets so no unity conversion will mess with the sonic quality
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private IEnumerator LoadAudioFromStreamingAssets(string fileName)
    {
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
        Debug.Log(filePath);
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(filePath, AudioType.WAV))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError($"Error loading audio file: {www.error}");
                yield break;
            }
            else
            {
                clip = DownloadHandlerAudioClip.GetContent(www);
            }
        }
        yield return new WaitForEndOfFrame();
        StartAudioPlayback();
    }

    /// <summary>
    /// Public function to begin transcode and automatic playback. Called once audio is loaded.
    /// </summary>
    public void StartAudioPlayback()
    {
        inBuffer = AudioClipToListOfFloatArrays(clip);
        ConvertAudioClipToMach1Spatial();

        LoadClipsIntoDecoder();
        decoder.LoadAudioData();
        decoder.PlayAudio();
    }

    /// <summary>
    /// Loads the individual clips into the Mach1 Unity Decoder for spatial playback
    /// </summary>
    private void LoadClipsIntoDecoder()
    {
        for(int i = 0; i < convertedClip.Count; i++)
        {
            decoder.audioClipMain[i] = convertedClip[i];
        }
    }

    /// <summary>
    /// Converts our audio clip to Float Arrays so we can use them
    /// </summary>
    /// <param name="clip"></param>
    /// <returns></returns>
    private List<float[]> AudioClipToListOfFloatArrays(AudioClip clip)
    {
        if (clip == null)
        {
            Debug.LogError("AudioClip is null.");
            return null;
        }

        int channelCount = clip.channels;
        int sampleCount = clip.samples;

        List<float[]> audioDataList = new List<float[]>(channelCount);
        for (int i = 0; i < channelCount; i++)
        {
            audioDataList.Add(new float[sampleCount]);
        }

        float[] tempData = new float[channelCount * sampleCount];
        clip.GetData(tempData, 0);

        for (int i = 0; i < sampleCount; i++)
        {
            for (int channel = 0; channel < channelCount; channel++)
            {
                audioDataList[channel][i] = tempData[channel + i * channelCount];
            }
        }

        return audioDataList;
    }

    /// <summary>
    /// Main transcode function
    /// </summary>
    public void ConvertAudioClipToMach1Spatial()
    {
        Mach1Transcode transcoder = new Mach1Transcode();

        Debug.Log("Setting up");

        transcoder.setInputFormat((int)inputFormat);
        Debug.Log("Input format: " + transcoder.getFormatName((int)inputFormat));

        transcoder.setOutputFormat((int)outputFormat);
        Debug.Log("Output format: " + transcoder.getFormatName((int)outputFormat));

        List<float[]> transcodedBuffers = new List<float[]>();
        int bufferFrames = inBuffer[0].Length;

        if (transcodedBuffers.Count != transcoder.getOutputNumChannels())
        {
            for (int c = 0; c < transcoder.getOutputNumChannels(); c++)
            {
                transcodedBuffers.Add(new float[bufferFrames]);
            }
        }

        //Always crashes here
        transcoder.processConversion(inBuffer, transcodedBuffers);

        convertedClip = FloatListToIndividualAudioClips(transcodedBuffers, 48000);
        
        Debug.Log("Converted.");

    }

    /// <summary>
    /// Convert back our Float List to the individual audio clips to be loaded into the Mach1 Decoder later
    /// </summary>
    /// <param name="audioDataList"></param>
    /// <param name="sampleRate"></param>
    /// <returns></returns>
    private List<AudioClip> FloatListToIndividualAudioClips(List<float[]> audioDataList, int sampleRate)
    {
        List<AudioClip> clips = new List<AudioClip>();

        int channelCount = audioDataList.Count;

        if (channelCount == 0)
        {
            Debug.LogError("No data in the audioDataList.");
            return clips;
        }

        int sampleCount = audioDataList[0].Length;

        for (int channel = 0; channel < channelCount; channel++)
        {
            AudioClip clip = AudioClip.Create($"Channel{channel}", sampleCount, 1, sampleRate, false);
            clip.SetData(audioDataList[channel], 0);
            clips.Add(clip);
        }

        return clips;
    }

    /// <summary>
    /// Handy utility function to list the input formats and their corresponding index numbers
    /// </summary>
    public void ListMach1InputFormats()
    {
        IntPtr m1obj = Mach1Transcode.Mach1TranscodeCAPI_create();

        for (int i = 0; i < 150; i++)
        {
            Mach1Transcode.Mach1TranscodeCAPI_setInputFormat(m1obj, i);

            IntPtr name = Mach1Transcode.Mach1TranscodeCAPI_getFormatName(m1obj, i);
            Debug.Log(i + ": " + Marshal.PtrToStringAnsi(name));
            //Marshal.FreeHGlobal(name);
        }
    }

}
