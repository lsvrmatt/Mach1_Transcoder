﻿//  Mach1 Spatial SDK
//  Copyright © 2017 Mach1, Corp. All rights reserved.

namespace Mach1
{
    public enum Mach1DecodeAlgoType : int
    {
        Mach1DecodeAlgoSpatial_8 = 0, Mach1DecodeAlgoHorizon_4, Mach1DecodeAlgoHorizonPairs, Mach1DecodeAlgoSpatial_12, Mach1DecodeAlgoSpatial_14, Mach1DecodeAlgoSpatial_32, Mach1DecodeAlgoSpatial_36, Mach1DecodeAlgoSpatial_48, Mach1DecodeAlgoSpatial_60
    };
}
